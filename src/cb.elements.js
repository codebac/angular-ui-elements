/**
 * The angular slider.
 *
 * @author JohnG
 */
angular.module("cb.elements", ["cb.elements.templates", "cb.elements.slider",
    "cb.elements.draggable"
]);
angular.module("cb.elements.templates", ["template/cb-slider/cb-slider.html"]);


/**
 * The draggable directive. This allows an element to be moved around the page.
 * The element must contain the cb-draggable attribute. With no value assigned,
 * the element will move along both the X and Y axis. If you wish to limit
 * the movement either vertically or horizontally, the attribute can have
 * the value "horizontal" or "vertical" to move only along the corresponding
 * axis.
 *
 * If the element receiving the draggable directive is not positioned
 * absolutely or relatively we force it to absolute.
 *
 * @author JohnG
 *
 * @attr limit Optional limits to be applied to the area the element can be
 * dragged. If present, but no value, it assumes movement is limited to the
 * parent element. Otherwise, a URI encodeded JSON string (not object) can
 * be passed in containing int values for maxX, minX, maxY, and minY.
 */
angular.module("cb.elements.draggable", [])
.directive("cbDraggable", function($document, $timeout) {
    return function(scope, element, attrs) {

        // determine how (if at all) the drag element should be limited
        scope.limit = false;
        if ("limit" in attrs) {

            // limit the element to the parent
            if (!attrs["limit"] || attrs["limit"] === "parent") {

                // give it a second to capture the element before assigning
                // limits
                $timeout(function() {
                    var elRadius = parseInt(element.outerWidth() / 2);
                    var $elParent = element.parent();

                    // calculate the limit based on the parent
                    scope.limit = {
                        minY : 0 - elRadius,
                        maxY : parseInt($elParent.outerHeight() - elRadius),
                        minX : 0 - elRadius,
                        maxX : parseInt($elParent.outerWidth() - elRadius),
                    };
                }, 0);
            } else {

                try {
                    scope.limit = angular.element.extend(
                        {
                            maxY : 100000,
                            minY : -100000,
                            maxX : 100000,
                            minX : -100000
                        },
                        JSON.parse(decodeURIComponent(attrs["limit"]))
                    );
                } catch (exc) {
                    // parsing failed, must be total crap
                    console.log("ERROR: Draggable limit attribute is not valid.");
                    scope.limit = false;
                }
            }
        }

        // the element must be positioned absolutely or relatively
        if (!element.css("position").match(/absolute|relative/)) {
            element.css("position", "absolute");
        }

        // default movement values
        var startX = 0, startY = 0, x = 0, y = 0;
        var moveX = true;
        var moveY = true;

        // determine the directions in which this can be dragged
        var dirs = attrs["cbDraggable"];
        if (dirs === "vertical") {
            moveX = false;
        } else if (dirs === "horizontal") {
            moveY = false;
        }

        element.on("mousedown", function(event) {
            // Prevent default dragging of selected content
            event.preventDefault();
            if (moveX) {
                startX = event.pageX - x;
            }
            if (moveY) {
                startY = event.pageY - y;
            }
            $document.on("mousemove", mousemove);
            $document.on("mouseup", mouseup);
        });

        function mousemove(event) {

            // calculate how much, if any, to move along the Y axis
            if (moveY) {
                y = event.pageY - startY;
                if (scope.limit && (y <= scope.limit.minY || y >= scope.limit.maxY)) {
                    return;
                }
            }

            // calculate how much, if any, to move along the X axis
            if (moveX) {
                x = event.pageX - startX;
                if (scope.limit && (x <= scope.limit.minX || x >= scope.limit.maxX)) {
                    return;
                }
            }

            // only manipulate the property that corresponds to the axis on
            // which we are moving
            if (moveX && moveY) {
                element.css({
                  "top"  : y + "px",
                  "left" : x + "px"
                });
            } else if (moveX) {
                element.css({
                  "left" : x + "px"
                });
            } else if (moveY) {
                element.css({
                  "top" : y + "px"
                });
            }
        }

        function mouseup() {
            $document.off("mousemove", mousemove);
            $document.off("mouseup", mouseup);
        }
    }
});

/**
 * The slider directive. In order to activate this, the element can either
 * must have the cb-slider attribute. If the attribute has no value
 * it is assumed that the slider is horizontal. If the attribute has a value
 * of "vertical" then the slider becomes vertical.
 *
 * @author JohnG
 */
angular.module("cb.elements.slider", ["cb.elements.draggable"])
.controller("SliderCtrl", ["$scope", "$attrs", function($scope, $attrs) {

    // determine the orientation of the slider (vertical or horizontal)
    $scope.orientation = $attrs.cbSlider
        ? $attrs.cbSlider
        : "horizontal";

}])
.directive("cbSlider", function() {
    return {
        restrict    : "A",
        controller  : "SliderCtrl",
        templateUrl : "template/cb-slider/cb-slider.html",
        scope       : {
            orientation : "&"
        }
    };
});

/**
 * The slider template placed into the templace cache so we don't have to use
 * an actual template file.
 *
 * @author JohnG
 */
angular.module("template/cb-slider/cb-slider.html", [])
.run(["$templateCache", function($templateCache) {

    $templateCache.put("template/cb-slider/cb-slider.html",
        "<div ng-class=\"['slider', orientation]\">\n" +
        "    <div class=\"slider-handle\" cb-draggable=\"{{orientation}}\" limit>\n" +
        "        <div class=\"slider-circle\"></div>\n" +
        "    </div>\n" +
        "</div>\n" +
        "");
}]);
